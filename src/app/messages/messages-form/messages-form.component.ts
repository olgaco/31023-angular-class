import { Component, OnInit, Output, EventEmitter } from '@angular/core'; // הוספת ערות תקשורת בין המסג'פורם לאבא שלו
import {FormGroup,FormControl } from '@angular/forms'; //הוספה
import { MessagesService } from "../messages.service";


@Component({
  selector: 'messagesForm',
  templateUrl: './messages-form.component.html',
  styleUrls: ['./messages-form.component.css']
})
export class MessagesFormComponent implements OnInit {
 
  @Output() addMessage:EventEmitter<any> = new EventEmitter<any>(); // הגדרת משתנה חדש מסוג איוונט אמיטר - סוג המידע שיועבר לא מוגדר- התשתית שתעביר לנו את המידע מאלמנט הבן לאלמנט האב
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>();

  service:MessagesService;
  msgform = new FormGroup({ // בניית מבנה נתונים בקוד המתאים אחד לאחד לטופס ההטמל. הנתונים ישמרו כמשתנים בקוד -> זהו אובייקט שדרכו תתבצע ה"תפירה" בטופס
    message:new FormControl(),
    user:new FormControl(),
  });

  sendData(){
    this.addMessage.emit(this.msgform.value.message);// פליטת תוכן ההודעה שנשלחה למסג'ס (מי שמעליי בהיררכיה) - התרחש אירוע של אד מסג'ס
    console.log(this.msgform.value); //  נשלח את הנתונים שהוכנסו בטופס לקונסול
    this.service.postMessage(this.msgform.value).subscribe(
      response => {
        console.log(response.json())
        this.addMessagePs.emit(); // מתזמנת את האירוע בניגוד לאופטימיסטי שמעדכן את המידע
      }
    )
  };

  constructor(service:MessagesService) { 
    this.service = service;
  }

  ngOnInit() {
  }

}
