import { Component, OnInit } from '@angular/core';
import {MessagesService} from './../messages/messages.service';

@Component({
  selector: 'messagesf',
  templateUrl: './messagesf.component.html',
  styleUrls: ['./messagesf.component.css']
})
export class MessagesfComponent implements OnInit {

  messages;
  constructor(private service:MessagesService) 
  { }

  ngOnInit() { // פונקציה שמתעוררת ברגע שהקומפוננט נוצר, לאחר הקונסטרקטור
    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.messages=response; // השמה של כל הנתונים שמגיעים מהשיטה לאובייקט בשם מסג'ס
    })
  }

}
