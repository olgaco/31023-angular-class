import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http'; 
import {HttpParams} from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import {environment} from './../../environments/environment';

import 'rxjs/Rx';

@Injectable()
export class MessagesService {
  http:Http;//http -> שם התכונה. Http-> סוג התכונה
  getMessages(){
      //return ['Message1','Message2','Message3','Message4'];
    //get messages from the SLIM rest API(DONT say DB!)
    let token = localStorage.getItem('token'); 
    let options = { // הוספת הטוקן להדר של הבקשה
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
    return this.http.get(environment.url+'messages',options);
  }

  getMessagesFire(){// הפונקציה תתחבר לדיבי של פיירבייס ותחזיר אובסרבל עליו נעשה סבסקרייב בקומפוננט
    return this.db.list('/messages').valueChanges(); // הפונקציה ליסט שולפת את כל התוכן מתוך הפויינטאנד והווליו צ'יינג יוצר את האובסרבל
  }

  getMessage(id){
    return this.http.get(environment.url+'messages/' +id);
  }

   postMessage(data) // דטה= נתונים של הטופס
   {
     let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
       headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
       })
     }
    let  params = new HttpParams().append('message', data.message); // פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
    return this.http.post(environment.url+ 'messages', params.toString(),options);

   }
   deleteMessage(key){
     return this.http.delete(environment.url+ 'messages/'+key);
   }

   login(credentials){// זה הסרבר ששולח לשרת את הנתונים של הלוגין לבדיקה
        let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
       })
     }
    let  params = new HttpParams().append('user', credentials.user).append('password',credentials.password); //  מתכוננים לשליחת הנתונים ,פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
    return this.http.post(environment.url+ 'auth', params.toString(),options).map(response=>{ // שליחה לראוט אוט
      let token = response.json().token; // המשתנה טוקן מכיל את הג'יידטי שהגיע מהשרת
      if (token) localStorage.setItem('token',token); // שמירה של הטוקן בלוקל סטורג'
      console.log(token);

    
   });
  }
  
  constructor(http:Http,private db:AngularFireDatabase) { //נוצר אובייקט עם תכונה מסוג היט טי טי פי ובנוסף יש אתחול של תכונה עם דיפנדנסיאיג'קשן שהתכונה של הדיבי תהיה מסוג אנגולר פיירבייס
    this.http = http;
   }

}
