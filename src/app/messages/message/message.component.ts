import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'; // בכדי לתפוס את האיידי מהראוטר
import {MessagesService} from './../messages.service';

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  message;
  constructor(private rout:ActivatedRoute, private service:MessagesService) { //גורם לאנגולר לראות שרצינו ליצור משתנה בשם ראוט ולייצר אובייקט חדש מסוג זה ולהפוך אותו לתכונה של המחקה הזאת=דיפנדנסי אינג'קשן ,האובייקט הוא ראוט מסוג אקטיביטד ראוט
    
   }

  ngOnInit() { // הקוד מופעל כרשר הקומפוננט נוצר= גם הקונסטרקטור פועל כשהקומפוננט נוצר
    this.rout.paramMap.subscribe(params=>{ //קריאת האיידי מהיואראל

      let id = params.get('id'); // שליפת האיידי
      console.log(id);
      this.service.getMessage(id).subscribe(response=>{ // יצירת אובסרבסל
        this.message = response.json();
        console.log(this.message);
      })
    }) 
  }

}
