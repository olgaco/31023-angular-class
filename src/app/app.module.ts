import {RouterModule} from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesService } from "./messages/messages.service";
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { LoginComponent } from './login/login.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {environment} from './../environments/environment';
import { MessagesfComponent } from './messagesf/messagesf.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessagesFormComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    MessageComponent,
    LoginComponent,
    MessagesfComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([ // בניה של הראוטס
      {path:'',component:MessagesComponent}, // הראוט הראשון זה דף הבית לוקלהוסט:4200
      {path:'users', component:UsersComponent},
      {path:'message/:id', component:MessageComponent},
      {path:'login',component:LoginComponent},
      {path:'messagesf',component:MessagesfComponent},
      {path:'**',component:NotFoundComponent}// צריך להופיע אחרון כי הוא תופס את כל מה שלא מוגדר ומעביר אליו

    ])

  ],
  providers: [
    MessagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
