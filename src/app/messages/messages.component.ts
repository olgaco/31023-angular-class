import { MessagesService } from './messages.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  
 // messages = ['Message1','Message2','Message3','Message4'];
 messages;//תכונה ריקה בשביל שנוכל להשתמש בה בקונסטרקטור
 messagesKeys= [];

  constructor(private service:MessagesService) {
    //let service = new MessagesService;--מחקנו כדי לאפשר יצירה אוטומטית מאנגולר
    service.getMessages().subscribe(
      response=>{//console.log(response)//arrow function .json() converts the string that we recived to jason
      this.messages= response.json();
      this.messagesKeys = Object.keys(this.messages);
   });
    
  }

  optimisticAdd(message){
   // console.log("addMessage worked" + message); בדיקה בלוג שהכל עובד

   var newKey = this.messagesKeys[this.messagesKeys.length-1] +1; //מניפולציה שמאפשרת את שליחת ההודעה- חייב להיות איידי להודעה ולכן אנחנו "ממציאים" מספר איידי זמני בישביל לשלוח את ההודעה
   var newMessageObject = {}; // יוצרים אובייקט חדש של מסג'ס
   newMessageObject['body'] = message; // יצירת אובייקט חדש של מסג'ס
   this.messages[newKey] = newMessageObject; // קישור ההודעה לאיידי הפקטיבי שיצרנו
    this.messagesKeys = Object.keys(this.messages); //הוספת האיבר האחרון של מסג'סקיס
  }

  pessimisticAdd(){

      this.service.getMessages().subscribe(response=>{
      //console.log(response)//arrow function .json() converts the string that we recived to jason
      this.messages= response.json();
      this.messagesKeys = Object.keys(this.messages);
   });
  }

  deleteMessage(key){ //מימוש אופטימיסטיק דליט
    console.log(key);
    let index = this.messagesKeys.indexOf(key); //מציאת המקום של הקי שנשלח בפונקציה
    this.messagesKeys.splice(index,1); // מחיקת הערך מהמערך


  //delete from server
    this.service.deleteMessage(key).subscribe(
      response=>console.log(response) // לצורך בדיקה
    );
  }

  ngOnInit() {
  }

}
