import {FormControl,FormGroup} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MessagesService } from "../messages/messages.service";
import { Router } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    invalid = false; // הגדרת משתנה לטובת בדיקה של הלוגין
    loginform = new FormGroup({ // בניית מבנה נתונים בקוד המתאים אחד לאחד לטופס ההטמל. הנתונים ישמרו כמשתנים בקוד -> זהו אובייקט שדרכו תתבצע ה"תפירה" בטופס
    user:new FormControl(),
    password:new FormControl(),
    
  });


  login(){ // מימוש פונקציית הלוגין
    this.service.login(this.loginform.value).subscribe(responser=>{
      this.router.navigate(['/']); // העברה לראוט הראשי של המסג'ס
    },error=>{this.invalid= true;
    }) //במידה ויש שגיאה 
  }

  logout(){ // הפונקציה מוחקת את הג'יידט מהסטורג'
    localStorage.removeItem('token');
    this.invalid= false; // 
  }
  constructor(private service:MessagesService, private router:Router) { } // גישה לסרביס + 



  ngOnInit() {
  }

}
