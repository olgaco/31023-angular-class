// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyD3PjR6wQkwuzPuCzUTR45WhD9NgaLwht0",
    authDomain: "messages-51e6e.firebaseapp.com",
    databaseURL: "https://messages-51e6e.firebaseio.com",
    projectId: "messages-51e6e",
    storageBucket: "messages-51e6e.appspot.com",
    messagingSenderId: "1094902255710"
  }
};
